<!DOCTYPE html>
<html lang="en">
<head>
  
  	<!-- ==============================================
	TITLE AND BASIC META TAGS
	=============================================== -->
    <meta charset="utf-8">
    <title>Gris - Creative Coming Soon Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
	
	<!-- ==============================================
	MOBILE METAS
	=============================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- ==============================================
	CSS
	=============================================== -->
    <link rel="stylesheet" href="css/elegant.css" type="text/css" />
   	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" id="pagestyle" />
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<!-- ==============================================
	FONTS
	=============================================== -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <!-- ==============================================
	JS
	=============================================== -->
	<script type="text/javascript" src="js/device.min.js"></script>
	
    <!-- ==============================================
	FAVICONS
	=============================================== -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/favicon/apple-touch-icon-144-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/favicon/apple-touch-icon-114-precomposed.html">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/favicon/apple-touch-icon-72-precomposed.html">
                    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-57-precomposed.html">
                                   <link rel="shortcut icon" href="images/favicon/favicon.png">

 <!-- DEMO Switcher
  	================================================== -->
    <link rel="stylesheet" href="js/styleswitcher/css/styleswitcher.css" />
    
</head>
<body>

	<!-- ==============================================
	PRELOADER
	=============================================== -->
	<div id="preloader">
    	<ul id="loader">
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div> <!-- End Preloader -->
	
	<!-- ==============================================
	IMAGE REPLACEMENT IN MOBILE AND TABLETS DEVICES
	=============================================== -->
	<div id="bgimg"></div>

	<!-- ==============================================
	HEADER
	=============================================== -->
	<div class="header_container">
		<a id="logo_main" href="#" class="header_logo" title="">
			<h1>Center Novation and Technology</h1>
		</a>
		<a class="map_button" id="map_button" title="">
			<span aria-hidden="true" data-icon="&#xe01d;"></span>
		</a>
		<a class="menu_button" id="menu_button" title="">
			<span aria-hidden="true" data-icon="&#x61;"></span>
		</a>
	</div>
	
	<!-- ==============================================
	BODY CONTENT
	=============================================== -->
	<div id="map" style="z-index: -100"></div>
    <div id="image-poster"></div>
    <div class="slideshow-pattern"></div>
    
   <div class="main_div">
    	


	    <div class="messages">
			<h2 id="message1">Мы <strong>креативные</strong> разработчики</h2>
			<h2 id="message2">Мы <strong>любим</strong> своё дело</h2>
			<h2 id="message3">Мы разрабатываем <strong>международные</strong> проекты</h2>
		</div>
	    
	   	<div class="main_content">
			<p class="intro"><strong>Подпишись на наши новости!</strong><br/>
                Расскажем Вам о наших новых проектах, поделимся интересной информацией!</p>
			<div id="mc_embed_signup">
				<form id="mc-form">
					<input id="mc-email" type="email" placeholder="Введите свой email..." class="form-control subs-input">
					<button type="submit" class="btn btn-default subs-submit">Сохранить</button>
					<label for="mc-email"></label>
				</form>
			</div>
		</div>	

	</div>
	
	<!-- ==============================================
	NAV CONTENT
	=============================================== -->
	<div class="nav_container" id="nav_container">
	
		<a class="nav_about" id="nav_about" title="">
			<p><span aria-hidden="true" data-icon="&#xe08b;"></span><br/>О НАС</p>
		</a>
		
		<a class="nav_contact" id="nav_contact" title="">	
			<p><span aria-hidden="true" data-icon="&#xe076;"></span><br/>СВЯЗЬ С НАМИ</p>
		</a>
		
		<div class="nav_content_container">
		
			<div class="nav_about_content">
				<h3>О НАС</h3>
				<hr>
				<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porttitor enim non purus tincidunt vulputate. Maecenas luctus tellus nec molestie bibendum.</h4>
				<img src="images/team.jpg" alt="Image about us" />
				<p>Curabitur pellentesque urna in velit viverra, in euismod nunc egestas. Morbi rutrum tellus et metus molestie facilisis. Duis porttitor nisl sagittis, malesuada leo sit amet, porttitor urna. Quisque lacinia hendrerit lectus, vitae gravida urna tincidunt a.</p>
			</div>
			
			<div class="nav_contact_content">
				<h3>СВЯЗЬ С НАМИ</h3>
				<hr>
				<p>
                    Хотите задать вопрос, или у Вас есть предложение?
                    Отправьте нам сообщение и мы обязательно Вам ответим!
                </p>
				<ul>
<!--					<li><p><span aria-hidden="true" data-icon="&#xe00b;"></span> +1 (123) 4567890</p></li>-->
					<li><p><a href="mailto:info@cnt.team"><span aria-hidden="true" data-icon="&#x76;"></span> info@cnt.team</a></p></li>
					<li><p><span aria-hidden="true" data-icon="&#xe01d;"></span> г. Москва, Болотная наб. 5с1</p></li>
				</ul>
				<form action="#" id="contactform">
                	<input type="text" name="name" placeholder="Имя">
					<input type="text" name="email" placeholder="E-mail">
					<textarea name="message" cols="35" rows="5" placeholder="Сообщение"></textarea>
					<button type="submit" value="Отправить">Отправить</button>
				</form>
			<p class="success-message-2"></p>
            <p class="error-message-2"></p> 
			</div>
			<a class="nav_about_close" id="nav_about_close" title=""><span aria-hidden="true" data-icon="&#x4d;"></span></a>
		</div>
		
	</div>	
	
	<a id="bgndVideo"  data-property="{videoURL:'https://www.youtube.com/watch?v=7T1MdtM61yY',containment:'body',autoPlay:true, mute:false, startAt:0,opacity:1,ratio:'16/9', addRaster:true}">My video</a>
	
	
	<!-- JAVASCRIPT
    ================================================== -->
    <!-- PLACED AT THE END OF THE DOCUMENT SO THE PAGES LOAD FASTER -->
    
    <!--[if lt IE 9]>
	<script src="js/ie/ie.min.js" type="text/javascript"></script>
	<![endif]-->
	<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="js/plugins.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCail-z_tlyyXkCD_Azw9W2kOflQ_oJYoA&sensor=true"></script>
    <script type="text/javascript" src="js/gmaps.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript" src="js/jquery.ajaxchimp.js"></script>
	<script type="text/javascript" src="js/ajaxchimp.js"></script>
	<script type="text/javascript" src="js/jquery.mb.YTPlayer.js"></script>
    <script type="text/javascript" src="js/video_youtube.js"></script>


</body>

</html>